<?php

namespace AppBundle\Repository;
use AppBundle\Model\Item;


class ItemRepository {
    private $items;

    public function __construct($entityManager)
    {
        $repo = $entityManager->getRepository('AppBundle:Product');
        $this->items = [];
        foreach ($repo->findAll() as $product) {
            $this->items[$product->getId()] = new Item($product->getId(), $product->getName(), $product->getPrice(), $product->getImage());
        }
        // $this->items = [
        //     1 => new Item(1, 'Ołówek', 199, 'pencil.jpg'),
        //     2 => new Item(2, 'Kostka Rubika', 259,'rubix.jpg'),
        //     3 => new Item(3, 'Gwizdek', 99, 'whistle.jpg')
        // ];
    }

    public function getAll()
    {
        return array_values($this->items);
    }

    public function getById($productId)
    {
        return $this->items[intval($productId)];
    }
}
