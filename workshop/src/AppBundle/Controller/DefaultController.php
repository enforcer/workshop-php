<?php

namespace AppBundle\Controller;

use AppBundle\Service\AuthService;
use AppBundle\Service\CartService;
use AppBundle\Model\Cart;
use AppBundle\Repository\ItemRepository;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $itemRepository = new ItemRepository($this->getDoctrine()->getManager());
        return $this->render('default/home.html.twig',
            [
                'products' => $itemRepository->getAll(),
                'cart' => $this->getCartFromSession($request)
        ]);
    }

    /**
     * @Route("/cart", name="cart")
     */
    public function cartAction(Request $request)
    {
        $auth = new AuthService();
        return $this->render('default/cart.html.twig',
            [
                'cart' => $this->getCartFromSession($request)
        ]);
    }

    /**
     * @Route("/addToCart/{productId}", name="addToCart")
     */
    public function addToCartAction(Request $request, $productId)
    {
        $cart = $this->getCartFromSession($request);

        $cartService = new CartService($this->getDoctrine()->getManager(), $cart);
        $cartService->addProductById($productId);

        $this->saveCartToSession($request, $cart);
        return $this->redirectToRoute('homepage');
    }

    // methods for retrieve/store current cart
    protected function getCartFromSession(Request $request)
    {
        $session = $request->getSession();
        $cartRaw = $session->get('cart');
        if ($cartRaw) {
            return unserialize($cartRaw);
        } else {
            return new Cart();
        }
    }

    protected function saveCartToSession(Request $request, Cart $cart)
    {
        $request->getSession()->set('cart', serialize($cart));
    }
}
