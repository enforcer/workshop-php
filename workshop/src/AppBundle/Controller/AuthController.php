<?php

namespace AppBundle\Controller;

use AppBundle\Service\AuthService;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class AuthController extends Controller
{
    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $request->getSession()->remove('user');

        $authService = new AuthService();
        $login = $request->request->get('login');
        $password = $request->request->get('password');

        if ($authService->authorize($login, $password)) {
            $user = (object) ['login' => $login];
            $request->getSession()->set('user', $user);
            return $this->redirectToRoute('homepage');
        } else {
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request)
    {
        $request->getSession()->remove('user');
        $request->getSession()->remove('cart');
        return $this->redirectToRoute('homepage');
    }
}
