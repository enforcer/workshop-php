<?php

namespace AppBundle\Service;

class ApiConsumer {

    public function getPostData($url, $data)
    {
        $paramsString = $this->createPostParamsString($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramsString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        $data = json_decode($response);
        return $data;
    }

    public function createPostParamsString($data)
    {
        $parts = [];
        foreach ($data as $key => $value) {
            $parts[] = sprintf('%s=%s', urlencode($key), urlencode($value));
        }
        return implode('&', $parts);
    }
}
