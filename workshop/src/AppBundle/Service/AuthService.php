<?php

namespace AppBundle\Service;

use AppBundle\Validator\LoginFormValidator;

class AuthService {
    const AUTH_URL = 'http://46.101.206.174:5000/';

    public function __construct()
    {
        $this->apiConsumer = new ApiConsumer();
    }

    public function authorize($login, $password)
    {
        $data = $this->apiConsumer->getPostData(static::AUTH_URL, ['login' => $login, 'password' => $password]);
        return !is_null($data);
    }

}
