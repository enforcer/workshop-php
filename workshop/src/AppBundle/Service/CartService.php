<?php

namespace AppBundle\Service;
use AppBundle\Repository\ItemRepository;

class CartService {
    private $entityManager;
    private $cart;

    public function __construct($entityManager, $cart)
    {
        $this->entityManager = $entityManager;
        $this->cart = $cart;
    }

    public function addProductById($productId)
    {
        $itemRepository = new ItemRepository($this->entityManager);
        $this->cart->addProduct($itemRepository->getById($productId));
    }
}
