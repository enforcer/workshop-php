<?php

namespace AppBundle\Tests\Model;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Model\Cart;
use AppBundle\Model\Item;

class DefaultControllerTest extends WebTestCase
{
    public function setUp()
    {
        $this->cart = new Cart();
    }

    public function testAddToCart()
    {
        $someItem = new Item(NULL, NULL, NULL, NULL);

        $this->cart->addProduct($someItem);
        $this->assertSame(1, $this->cart->count());
    }

    public function testCartIsIterable()
    {
        $someItemFirst = new Item(1, NULL, NULL, NULL);
        $someItemSecond = new Item(2, NULL, NULL, NULL);

        $this->cart->addProduct($someItemFirst);
        $this->cart->addProduct($someItemSecond);

        foreach ($this->cart as $key => $value) {
            $this->assertSame($someItemFirst, $value);
            break;
        }
    }
}
