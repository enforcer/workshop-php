<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\ApiConsumer;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ApiConsumerTest extends WebTestCase
{
    public function setUp()
    {
        $this->service = new ApiConsumer();
    }

    public function testCreatePostStringForEmptyData()
    {
        $data = [];
        $result = $this->service->createPostParamsString($data);
        $this->assertSame('', $result);
    }

    public function testCreatePostStringForLoginAndPassword()
    {
        $data = ['login' => 'loginstring', 'password' => 'secret123'];
        $result = $this->service->createPostParamsString($data);
        $this->assertSame('login=loginstring&password=secret123', $result);
    }

    /**
     * @group integration
     */
    public function testGivesEmptyReponseForInvalidData()
    {
        $this->markTestIncomplete();
    }
}


