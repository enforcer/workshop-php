<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\AuthService;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthServiceTest extends WebTestCase
{
    public function setUp()
    {
        $this->service = new AuthService();
    }

    public function getApiStub($returnValue = NULL)
    {
        $apiConsumerStub = $this->getMockBuilder('AppBundle\Service\ApiConsumer')->getMock();
        $apiConsumerStub->method('getPostData')->willReturn($returnValue);
        return $apiConsumerStub;
    }

    public function testValidCredentialsAllowToAuthenticateSuccessfully()
    {
        $this->service->apiConsumer = $this->getApiStub(new \stdClass());
        $login = 'somelogin';
        $password = 'somesecret';

        $result = $this->service->authorize($login, $password);

        $this->assertSame(true, $result);
    }

    public function testInvalidCredentialsAreNotSendToAPI()
    {
        $this->markTestIncomplete();
        $this->service->apiConsumer = $this->getApiStub();
        $this->service->apiConsumer->expects($this->never())->method('getPostData');
        $login = '';
        $password = NULL;

        $result = $this->service->authorize($login, $password);
    }
}



