<?php

namespace AppBundle\Model;
use AppBundle\Model\Item;

class Cart implements \Iterator {
    private $items;
    private $position;

    public function __construct()
    {
        $this->position = 0;
        $this->items = [];
    }

    public function addProduct(Item $item)
    {
        $this->items[] = $item;
    }

    public function count()
    {
        return \count($this->items);
    }

    public function productCount()
    {
        $unique = [];
        foreach ($this->items as $item) {
            if (!in_array($item->getId(), $unique)) {
                $unique[] = $item->getId();
            }
        }
        return \count($unique);
    }

    // Iterables
    public function current()
    {
        return $this->items[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        $this->position++;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function valid()
    {
        return $this->position < \count($this->items);
    }
}
