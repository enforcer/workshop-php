<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

use AppBundle\Entity\Product;
use AppBundle\Model\Cart;
use AppBundle\Service\CartService;
use AppBundle\Repository\ItemRepository;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct($session, $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->insertedProducts = [];
    }

    /**
     * @Given there are products:
     */
    public function thereAreProducts(TableNode $table)
    {
        $hash = $table->getHash();

        $products = [];
        foreach ($hash as $row) {
            $product = new Product();
            $product->setName($row['name'])->setPrice($row['price'])->setImage($row['image']);
            $this->entityManager->persist($product);
            $products[] = $product;
        }
        $this->entityManager->flush();

        foreach ($products as $product) {
            $this->insertedProducts[$product->getId()] = $product;
        }
    }

    /**
     * @Given the cart is empty
     */
    public function theCartIsEmpty()
    {
        $this->cart = new Cart();
    }

    /**
     * @When I add :productName to cart :nTimes times
     */
    public function iAddToCartTimes($productName, $nTimes)
    {
        $prefiltered = array_filter($this->insertedProducts, function($product) use($productName)
            { return $product->getName() === $productName;});
        $product = array_pop($prefiltered);
        assert($product);

        $cartService = new CartService($this->entityManager, $this->cart);
        for ($i = 0; $i < intval($nTimes); $i++) {
            $cartService->addProductById($product->getId());
        }
    }

    /**
     * @Then I should get :count for products
     */
    public function iShouldGetForProducts($count)
    {
        assert($this->cart->count() === intval($count));
    }

    /**
     * @Then I should get :count for items count
     */
    public function iShouldGetForItemsCount($count)
    {
        assert($this->cart->productCount() === intval($count));
    }

     /**
      * @AfterScenario
      */
     public function cleanDB()
     {
        $repo = $this->entityManager->getRepository('AppBundle:Product');
        foreach (array_keys($this->insertedProducts) as $productId) {
            $entity = $repo->findOneById($productId);
            $this->entityManager->remove($entity);
        }
        $this->entityManager->flush();
     }
}
