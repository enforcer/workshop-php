Feature: Adding products to cart

Scenario: Add one product to cart
 Given there are products:
  | name   | price | image      |
  | pencil |   299 | pencil.jpg |
 And the cart is empty
 When I add "pencil" to cart "2" times
 Then I should get "2" for products
 And I should get "1" for items count
