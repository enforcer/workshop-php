<?php

require_once('vendor/autoload.php');
require_once('kata.php');


class BowlingGameTest extends PHPUnit_Framework_TestCase
{
    public function testLameGame()
    {
        $game = new Game();
        for ($i = 0; $i < 20; $i++) {
            $game->roll(0);
        }
        $this->assertSame(0, $game->score());
    }
}
